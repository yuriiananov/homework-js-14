"use strict";

function toggleTheme() {
  const themeStyle = document.getElementById("theme-style");
  const currentTheme = themeStyle.getAttribute("href");
  const newTheme = currentTheme === "./styles/styles_light_theme.css" ? "./styles/styles_dark_theme.css" : "./styles/styles_light_theme.css";

  themeStyle.setAttribute("href", newTheme);
  localStorage.setItem("selectedTheme", newTheme);
}

const themeToggleBtn = document.getElementById("theme-toggle-btn");
themeToggleBtn.addEventListener("click", toggleTheme);

const selectedTheme = localStorage.getItem("selectedTheme");
if (selectedTheme) {
  const themeStyle = document.getElementById("theme-style");
  themeStyle.setAttribute("href", selectedTheme);
}